<?php
    // Start the session
    session_start();
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 32; $i++)
    {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
  	//Genrate Random Key
  ?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="container">
        <form class="animate" action="/action_page.php" method="post" id="login_form" name="login_form">
            <input type="hidden" name="enckey" id="enckey" value="<?php echo $randomString; ?>">
            <div class="imgcontainer">
                <img src="images/logo.jpg" alt="Avatar" class="avatar">
            </div>
            <div class="form-group">
                <label for="username"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="username" required>
            </div>
            <div class="form-group">
                <label for="password"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password" required>
            </div>
            <div class="form-group">
                <button type="submit">Login</button>
            </div>
        </form>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"
        integrity="sha512-nOQuvD9nKirvxDdvQ9OMqe2dgapbPB7vYAMrzJihw5m+aNcf0dX53m6YxM4LgA9u8e9eg9QX+/+mPu8kCNpV2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="js/encryption.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
          $("#login_form").submit(function(e) {
              e.preventDefault();
              let encryption = new Encryption();
              var nonceValue = $('#enckey').val();
              var form_data = $('#login_form').serializeArray().reduce(function(obj, item) {
                  if (item.name != "enckey") {
                      obj[item.name] = encryption.encrypt(item.value, nonceValue);
                  } else {
                      obj[item.name] = item.value;
                  }
                  return obj;
              }, {});
              $.ajax({
                  type: "POST",
                  url: 'ajax-login.php',
                  data: form_data, // serializes the form's elements.
                  success: function(data) {
                      alert(data); // show response from the php script.
                  }
              });
          });

      });
    </script>
</body>

</html>